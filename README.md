# Optimization of Quadratic Problems via Singular Value Decomposition

This is an implementation of the [SVD][] optimization procedure as a
reinforcement learning algorithm. The API has been inspired by [Stable
Baselines 3][], but someone simplified to make the code more readable.

In addition, this package is compatible with the [MLP Model API][]; instances
of `SvdOptimizerModel` can be stored and fetched via the [MLP Client][].

[SVD]: https://uspas.fnal.gov/materials/05UCB/6_SVD.pdf
[Stable Baselines 3]: https://stable-baselines3.readthedocs.io/
[MLP Model API]: https://gitlab.cern.ch/acc-co/machine-learning-platform/mlp-model-api/
[MLP Client]: https://gitlab.cern.ch/acc-co/machine-learning-platform/mlp-client/

## Usage

To install this package from the [Acc-Py Repository][], simply run the
following line while on the CERN network:

[Acc-Py Repository]: https://wikis.cern.ch/display/ACCPY/Getting+started+with+Acc-Py

```shell
pip install cernml-svd
```

To use the source repository, you must first install it as well:

```shell
git clone https://gitlab.cern.ch/geoff/optimizers/cernml-svd.git
cd ./cernml-svd/
pip install .
```

Usage example:

```python
import cern_awake_env.simulation
from cernml.coi import make
from cernml.svd import SvdOptimizer

env = make("AwakeSimEnvH-v0")
agent = SvdOptimizer(env)
agent.learn(total_timesteps=env.action_space.shape[0])

obs, done = env.reset(), False
while not done:
    action, _ = agent.predict(obs)
    obs, reward, done, _ = env.step(action)
print("Final reward:", reward)
```
