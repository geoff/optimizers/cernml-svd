"""Tests for the :mod:`_impl` module."""

# pylint: disable = missing-function-docstring

import numpy as np
from cernml.rltools.buffers import Step, StepBuffer

from .._impl import calculate_response_matrix, find_action


def make_random_data(num_actions: int) -> StepBuffer:
    return StepBuffer(
        Step(
            np.random.uniform(-1.0, 1.0, size=num_actions),
            np.random.uniform(-1.0, 1.0, size=num_actions),
            0.0,
            np.random.uniform(-1.0, 1.0, size=num_actions),
            False,
        )
        for _ in range(num_actions)
    )


def test_svd_works() -> None:
    for _ in range(100):
        data = make_random_data(3)
        response = calculate_response_matrix(data)
        for step in data:
            delta_obs = step.next_obs - step.obs
            action = find_action(response, delta_obs)
            assert np.allclose(-action, step.action)


def test_avoid_large_actions() -> None:
    singular_values = np.arange(1.0, 4.0)
    response = np.diag(singular_values)
    step = np.ones(3)
    for i in range(4):
        step_size = 1.0 + float(i)
        solution = 1 / singular_values
        solution[:i] = 1 / 999
        solution *= -step_size
        action = find_action(response, step_size * step, max_action_size=1.1)
        assert np.allclose(action, solution), i


def test_calculate_identity() -> None:
    identity = np.diag(np.ones(3))
    data = StepBuffer(Step(np.zeros(3), row, 0.0, row, False) for row in identity)
    response = calculate_response_matrix(data)
    assert np.array_equal(response, identity)


def test_shift_invariant() -> None:
    for _ in range(100):
        data = make_random_data(3)
        first_response = calculate_response_matrix(data)
        shift = np.random.uniform(-1.0, 1.0, size=len(data))
        data = StepBuffer(
            Step(
                step.obs + shift,
                step.action,
                step.reward,
                step.next_obs + shift,
                step.done,
            )
            for step in data
        )
        second_response = calculate_response_matrix(data)
        assert np.array_equal(first_response, second_response)
