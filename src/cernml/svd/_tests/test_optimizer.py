"""Tests for the _optimizer module."""

# pylint: disable = missing-function-docstring

import typing as t
from pathlib import Path

import gym
import gym.wrappers
import numpy as np

from .._optimizer import SvdOptimizer


class LinearEnv(gym.Env):
    """Environment with a randomized response matrix."""

    metadata = {
        "render.modes": ["ansi"],
    }
    action_space = gym.spaces.Box(-1, 1, (3,), dtype=float)
    observation_space = gym.spaces.Box(-1, 1, (3,), dtype=float)

    def __init__(self) -> None:
        self.action_scale = 0.5
        self.response_matrix = np.random.uniform(-1, 1, size=(3, 3))
        self.response_matrix /= np.linalg.det(self.response_matrix)
        self.steerer_settings = np.random.uniform(-0.5, 0.5, size=3)
        self.golden_trajectory = np.random.uniform(-0.5, 0.5, size=3)

    def __repr__(self) -> str:
        return f"{type(self).__name__}({self.steerer_settings})"

    def reset(self) -> np.ndarray:
        self.steerer_settings = np.random.uniform(-0.5, 0.5, size=3)
        return self._get_obs()

    def step(
        self, action: np.ndarray
    ) -> t.Tuple[np.ndarray, float, bool, t.Dict[str, t.Any]]:
        self.steerer_settings += self.action_scale * action
        obs = self._get_obs()
        reward = -np.sqrt(np.mean(np.square(obs)))
        done = not -0.5 <= reward <= -0.01
        info = {"success": reward > -0.01}
        return obs, reward, done, info

    def _get_obs(self) -> np.ndarray:
        bpm_readings = self.response_matrix @ self.steerer_settings
        obs = bpm_readings - self.golden_trajectory
        return obs

    def render(self, mode: str = "human") -> t.Any:
        if mode == "ansi":
            return repr(self)
        return super().render(mode)


def test_steering() -> None:
    # Disable max_action_size – otherwise we can get into situations
    # were the algorithm outputs an action ~0.0 and we get stuck.
    agent = SvdOptimizer(LinearEnv(), max_action_size=float("inf")).learn(3)
    env = gym.wrappers.TimeLimit(agent.get_env(), 1)
    print(agent.response_matrix)
    for _ in range(100):
        obs = env.reset()
        done = False
        while not done:
            action, _ = agent.predict(obs)
            print(obs, action)
            obs, reward, done, info = env.step(action)
        assert np.allclose(obs, 0.0), info
        assert reward > -1e-10, info
        assert info == {"success": True, "TimeLimit.truncated": False}


def test_reconstruct_response() -> None:
    env = LinearEnv()
    agent = SvdOptimizer(env)
    assert agent.response_matrix is None
    result = agent.learn(3)
    assert result is agent
    assert agent.response_matrix is not None
    assert np.allclose(agent.response_matrix, env.action_scale * env.response_matrix)


def test_load_save(tmp_path: Path) -> None:
    trained_agent = SvdOptimizer(LinearEnv()).learn(3)
    assert trained_agent.response_matrix is not None
    trained_agent.save(tmp_path / "agent.zip")
    loaded_agent = SvdOptimizer.load(tmp_path / "agent.zip")
    assert loaded_agent.env is None
    assert loaded_agent.response_matrix is not None
    assert np.array_equal(loaded_agent.response_matrix, trained_agent.response_matrix)
    assert loaded_agent.observation_space == trained_agent.observation_space
    assert loaded_agent.action_space == trained_agent.action_space
    assert loaded_agent.action_scale == trained_agent.action_scale
    assert loaded_agent.verbose == trained_agent.verbose
    assert loaded_agent.max_action_size == trained_agent.max_action_size
