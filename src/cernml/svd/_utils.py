"""Utilities for data collection."""

import typing as t

import gym
import numpy as np


class LogSteps(gym.Wrapper):
    """Env wrapper that logs progress.

    Args:
        env: The environment to wrap.
        num_timesteps: The initial step count.
        total_timesteps: The expected final step count.
        log_interval: Interval between log messages.
        verbose: If greater than zero, print a progress report on every
            ``log_interval``-th step.

    Example:

        >>> class SillyEnv(gym.Env):
        ...     observation_space = gym.spaces.Box(-1, 1, (2,))
        ...     action_space = gym.spaces.Box(-1, 1, (2,))
        ...     def reset(self):
        ...         return self.observation_space.sample()
        ...     def step(self, _):
        ...         return self.observation_space.sample(), 0.0, False, {}
        >>> env = LogSteps(SillyEnv(), total_timesteps=10, log_interval=2)
        >>> for _ in range(12):
        ...     _ = env.step(env.action_space.sample())
        taking step 2/10 ...
        taking step 4/10 ...
        taking step 6/10 ...
        taking step 8/10 ...
        taking step 10/10 ...
        taking step 12/10 ...
    """

    def __init__(
        self,
        env: gym.Env,
        *,
        total_timesteps: int,
        num_timesteps: int = 0,
        log_interval: int = 1,
        verbose: int = 1,
    ) -> None:
        super().__init__(env)
        self.num_timesteps = num_timesteps
        self.total_timesteps = total_timesteps
        self.log_interval = log_interval
        self.verbose = verbose

    def step(
        self, action: np.ndarray
    ) -> t.Tuple[np.ndarray, float, bool, t.Dict[str, t.Any]]:
        self.num_timesteps += 1
        if self.verbose > 0 and self.num_timesteps % self.log_interval == 0:
            print(f"taking step {self.num_timesteps}/{self.total_timesteps} ...")
        return super().step(action)


class ActionSpaceScanner:
    """An RL agent that simply scans through an action space.

    Args:
        space: The action space to scan.
        scale: The step size of each action.

    On the first step, this agent predicts an action of size ``scale``
    on the first axis and zero on all other axes. On the second step, it
    undoes its first step and, simultaneously, makes a step of size
    ``scale`` on the second axis. This continues in a cyclic manner.

    Note that because of the implicit "undo", the very first step is
    different from all subsequent ones in that the returned action has
    only a single non-zero entry.

    Example:

        >>> space = gym.spaces.Box(-1, 1, shape=(3,), dtype=float)
        >>> agent = ActionSpaceScanner(space, 1.0)
        >>> agent.predict(np.zeros(10))
        (array([1., 0., 0.]), None)
        >>> agent.predict(np.zeros(10))
        (array([-1.,  1.,  0.]), None)
        >>> agent.predict(np.zeros(10))
        (array([ 0., -1.,  1.]), None)
        >>> agent.predict(np.zeros(10))
        (array([ 1.,  0., -1.]), None)
        >>> agent.predict(np.zeros(10))
        (array([-1.,  1.,  0.]), None)
    """

    # pylint: disable = too-few-public-methods

    def __init__(self, space: gym.spaces.Box, scale: float) -> None:
        if len(space.shape) != 1:
            raise TypeError(f"space must be 1-D, but has shape {space.shape} instead")
        if not scale > 0.0:
            raise ValueError(f"action scale must be >0: {scale}")
        self.shape = space.shape
        self.dtype = space.dtype
        self.next_action: t.Optional[np.ndarray] = None
        self.scale = scale

    def predict(
        self, _obs: np.ndarray, _state: t.Optional[np.ndarray] = None
    ) -> t.Tuple[np.ndarray, None]:
        """Predict the next action.

        This agent always ignores the given observation.
        """
        if self.next_action is None:
            # This action: [1, 0, 0, …].
            action = np.zeros(self.shape, dtype=self.dtype)
            action[0] = self.scale
            # Next action: [-1, 1, 0, …].
            self.next_action = np.roll(action, 1)
            self.next_action[0] = -self.scale
        else:
            # This action: […, -1, 1, 0, …].
            action = self.next_action
            # Next action: […, 0, -1, 1, …].
            self.next_action = np.roll(self.next_action, 1)
        return action, None
