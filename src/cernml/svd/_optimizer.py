"""Implementation of the optimizer class."""

import random
import typing as t

import gym
import mlp_model_api
import numpy as np
from cernml.rltools import buffers, envloop

from . import _impl, _persist, _utils

if t.TYPE_CHECKING:
    # pylint: disable = unused-import
    import pathlib


class NotEnoughTimesteps(Exception):
    """Need more timesteps to calculate response matrix."""


Self = t.TypeVar("Self", bound="SvdOptimizer")


class SvdOptimizer:

    """Singular value decomposition of response matrices.

    Args:
        env: The environ,ent to learn from. Can be str for registered
            Gym envs. Can be None for loading trained models.
        action_scale: The magnitude of the steps to take during
            the learning phase. The steps are necessary to estimate the
            response matrix.
        verbose: The verbosity level: 0 for no output, 1 for training
            information, 2 for debug information.
        max_action_size: Limit on the absolute value of any action array
            returned by this optimizer. Each array element is tested
            individually. If the action exceeds this limit, the SVD
            procedure is repeated with a tweaked response matrix. This
            parameter must be greater than zero and not NaN. It may be
            infinite to disable the limit.
    """

    # pylint: disable = too-many-instance-attributes

    SUPPORTED_ACTION_SPACES: t.ClassVar[t.Tuple[t.Type[gym.Space], ...]] = (
        gym.spaces.Box,
    )

    def __init__(
        self,
        env: t.Union[gym.Env, str, None],
        action_scale: float = 0.5,
        verbose: int = 0,
        max_action_size: float = 2.0,
    ) -> None:
        # Note to developers: Any additional configs should be:
        #   - passed to this method,
        #   - be given a sensible default value,
        #   - stored as instance attributes,
        #   - documented in the class docstring.
        if np.isnan(max_action_size) or max_action_size <= 0.0:
            raise ValueError(f"invalid max_action_size: {max_action_size}")
        self.action_scale = action_scale
        self.verbose = verbose
        self.max_action_size = max_action_size
        # Non-config attributes follow.
        self.env: t.Optional[gym.Env] = None
        self.observation_space: t.Optional[gym.Space] = None
        self.action_space: t.Optional[gym.Space] = None
        self.num_timesteps = 0
        self._total_timesteps = 0
        self.response_matrix: t.Optional[np.ndarray] = None
        # Initialization from env.
        if env is not None:
            if isinstance(env, str):
                self.env = gym.make(env)
            else:
                self.env = env
            assert self.env is not None
            self.observation_space = self.env.observation_space
            self.action_space = self.env.action_space
            assert isinstance(self.action_space, self.SUPPORTED_ACTION_SPACES), (
                f"the algorithm only supports {self.SUPPORTED_ACTION_SPACES} "
                f"as action spaces, not {self.action_space}"
            )

    def get_env(self) -> t.Optional[gym.Env]:
        """Return the current environment. (Can be None.)"""
        return self.env

    def set_env(self, env: gym.Env) -> None:
        """Return the current environment. (Can be None.)"""
        if self.observation_space:
            if env.observation_space != self.observation_space:
                raise ValueError(
                    f"observation spaces do not match: "
                    f"{env.observation_space} != {self.observation_space}"
                )
        if self.action_space:
            if env.action_space != self.action_space:
                raise ValueError(
                    f"action spaces do not match: "
                    f"{env.action_space} != {self.action_space}"
                )
        self.observation_space = env.observation_space
        self.action_space = env.action_space
        self.env = env

    def learn(
        self,
        total_timesteps: int,
        log_interval: int = 100,
        reset_num_timesteps: bool = True,
    ) -> "SvdOptimizer":
        """Return a trained model.

        Args:
            total_timesteps: The total number of samples (env steps) to
                train on. This should be exactly the size of the action
                space. Smaller values raise :exc:`NotEnoughTimesteps`;
                greater values are silently replaced with the size of
                the action space.
            log_interval: The number of timesteps before logging.
            reset_num_timesteps: If True, reset the current timestep
                number (used in logging).

        Returns:
            The trained model.

        Raises:
            ValueError: if called without an environment to learn from.
            NotEnoughTimesteps: if `total_timesteps` is too small.
        """
        if (
            self.observation_space is None
            or self.action_space is None
            or self.env is None
        ):
            raise ValueError(
                "need an env to learn; pass it in __init__() or call self.set_env()"
            )
        expected_total_timesteps = np.prod(self.action_space.shape)
        if total_timesteps < expected_total_timesteps:
            raise NotEnoughTimesteps(
                f"need at least {expected_total_timesteps}, got {total_timesteps}"
            )
        if total_timesteps > expected_total_timesteps:
            if self.verbose > 0:
                print(
                    "only performing",
                    expected_total_timesteps,
                    "instead of",
                    total_timesteps,
                    "timesteps",
                )
            total_timesteps = expected_total_timesteps
        if reset_num_timesteps:
            self.num_timesteps = 0
        else:
            total_timesteps += self.num_timesteps
        self._total_timesteps = total_timesteps
        # TODO: Ideally, we'd allow calling `learn()` multiple times to
        # collect data in batches. For this, our data collection loop
        # would have to allow _not_ starting the loop by calling
        # env.reset(). It doesn't do that yet, so we'll have to
        # implement this ability later.
        data = self._collect_data(log_interval)
        self.response_matrix = _impl.calculate_response_matrix(data)
        return self

    def _collect_data(self, log_interval: int) -> buffers.StepBuffer:
        max_timesteps = self._total_timesteps - self.num_timesteps
        env = _utils.LogSteps(
            self.env,
            num_timesteps=self.num_timesteps,
            total_timesteps=self._total_timesteps,
            log_interval=log_interval,
            verbose=self.verbose,
        )
        agent = _utils.ActionSpaceScanner(self.action_space, self.action_scale)
        data = envloop.collect_data(agent, env, max_timesteps=max_timesteps)
        # TODO: Ideally, we'd increase num_timesteps while collecting
        # data. It's not quite clear yet how best to implement this
        # without a circular dependency between LogSteps and
        # SvdOptimizer. Maybe by passing a callback?
        self.num_timesteps += len(data)
        return data

    def predict(
        self,
        observation: np.ndarray,
        state: t.Optional[np.ndarray] = None,
        mask: t.Optional[np.ndarray] = None,
        deterministic: bool = False,
    ) -> t.Tuple[np.ndarray, None]:
        """Get the model's action from an observation.

        Usually, the observation is a set of BPM readings and the action
        is a set of corrections on steerer settings.

        Args:
            observation: The input observation.
            state: The last state. (Should be None; only used in
                recurrent policies, which are not supported by this
                algorithm.)
            mask: The last masks. (Should be None; only used in
                recurrent policies, which are not supported by this
                algorithm.)
            deterministic: Ignored; this algorithm always returns
                deterministic actions.

        Returns:
            The prediction optimal action, and None (would be the next
            state if this algorithm supported recurrent policies).
        """
        # Note: We use neither state nor mask nor deterministic, but we
        # allow passing them to be more similar to Stable Baselines 3.
        # pylint: disable = unused-argument
        if self.response_matrix is None:
            raise ValueError(
                "cannot predict on an untrained algorithm; call "
                "self.learn() or cls.load() first"
            )
        action = _impl.find_action(
            response_matrix=self.response_matrix,
            obs=observation,
            max_action_size=self.max_action_size,
        )
        return action, None

    def set_random_seed(self, seed: t.Optional[int] = None) -> None:
        """Set the seed of all PRNGs.

        This modifies global state (Python's and Numpy's PRNG seed) as
        well as local state (the env's and the action space's seed).
        """
        if seed is None:
            return
        random.seed(seed)
        np.random.seed(seed)
        if self.action_space:
            self.action_space.seed(seed)
        if self.env:
            self.env.seed(seed)

    @classmethod
    def load(
        cls: t.Type[Self],
        path: t.Union[str, "pathlib.Path", t.BinaryIO],
        env: t.Optional[gym.Env] = None,
        **kwargs: t.Any,
    ) -> Self:
        """Load the model from a zip file.

        Args:
            path: The path from where to load the model.
            env: The new environment to run the model on. (Can be None
                if only predictions are needed.)
            kwargs: Extra attributes to set on the model after loading.

        Returns:
            The loaded model.
        """
        data, params = _persist.load_from_zip_file(path)
        if env is None:
            env = data.get("env", None)
        model = cls(env)
        model.__dict__.update(data)
        model.__dict__.update(kwargs)
        model.response_matrix = params.get("response_matrix", None)
        return model

    def save(
        self,
        path: t.Union[str, "pathlib.Path", t.BinaryIO],
        exclude: t.Optional[t.Iterable[str]] = None,
        include: t.Optional[t.Iterable[str]] = None,
    ) -> None:
        """Save all attributes and parameters to a zip file.

        Args:
            path: Path the the file where the agent should be saved.
            exclude: Names of parameters that should be excluded in
                addition to the default ones.
            include: Names of parameters that should be included despite
                being on the exclusion list.
        """
        exclude_set = set(self._excluded_save_params())
        if exclude is not None:
            exclude_set = exclude_set.union(exclude)
        if include is not None:
            exclude_set = exclude_set.difference(include)
        # Copy parameter list so we don't mutate the original dict.
        data = self.__dict__.copy()
        for name in exclude_set:
            data.pop(name, None)
        params_to_save: t.Dict[str, np.ndarray] = {}
        if self.response_matrix is not None:
            params_to_save["response_matrix"] = self.response_matrix
        _persist.save_to_zip_file(path, data=data, params=params_to_save)

    @staticmethod
    def _excluded_save_params() -> t.List[str]:
        return [
            "env",
            "response_matrix",
        ]


class SvdOptimizerModel(mlp_model_api.MlpModel):
    """`MLP`_ adapter of SvdOptimizer.

    .. _`MLP`: https://gitlab.cern.ch/acc-co/machine-learning-platform/mlp-model-api/
    """

    model: SvdOptimizer

    def __init__(self) -> None:
        self.model = SvdOptimizer(None)

    def predict(self, input_data: t.Dict[str, np.ndarray]) -> t.Dict[str, np.ndarray]:
        observation = input_data[mlp_model_api.INPUTS]
        action, _ = self.model.predict(observation)
        return {mlp_model_api.OUTPUTS: action}

    def export_parameters(self, parameters_target: "pathlib.Path") -> None:
        self.model.save(parameters_target)

    def load_parameters(self, parameters_src: "pathlib.Path") -> None:
        self.model = SvdOptimizer.load(parameters_src)
