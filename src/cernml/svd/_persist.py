"""Functions for saving and loading models."""

import json
import os
import pathlib
import typing as t
import warnings
import zipfile

import gym
import numpy as np

try:
    import importlib.metadata as importlib_metadata
except ImportError:
    import importlib_metadata  # type: ignore[no-redef]


def load_from_zip_file(
    path: t.Union[str, pathlib.Path, t.BinaryIO]
) -> t.Tuple[t.Dict[str, t.Any], t.Dict[str, np.ndarray]]:
    """Save model data to a zip file.

    Args:
        path: Where to store the model. May be either a path or a file
            opened for reading in binary mode.

    Returns:
        A tuple ``(data, params)`` with the restored config data and
        trainable parameters. See :py:func:`save_to_zip_file()` for
        their meaning.
    """
    with _open(path, "rb") as raw_file, zipfile.ZipFile(raw_file) as archive:
        namelist = archive.namelist()
        data: t.Dict[str, t.Any] = {}
        if "data" in namelist:
            json_string = archive.read("data").decode()
            data = json.loads(json_string, cls=NumpyGymJsonDecoder)
        params: t.Dict[str, np.ndarray] = {}
        for param_path in namelist:
            name, ext = os.path.splitext(param_path)
            if ext != ".npy":
                continue
            with archive.open(param_path) as param_file:
                params[name] = np.load(param_file, allow_pickle=False)
    return data, params


def save_to_zip_file(
    path: t.Union[str, pathlib.Path, t.BinaryIO],
    data: t.Dict[str, t.Any],
    params: t.Dict[str, np.ndarray],
) -> None:
    """Save model data to a zip file.

    Args:
        path: Where to store the model. May be either a path or a file
            opened for writing in binary mode.
        data: Class configuration parameters to be stored.
        params: Trainable model parameters to be stored.
    """
    try:
        version = importlib_metadata.version("cernml-svd")
    except importlib_metadata.PackageNotFoundError as exc:
        warnings.warn(f"Could not determine cernml-svd version; reason: {exc}")
        version = "<unknown>"
    serialized_data = json.dumps(data, cls=NumpyGymJsonEncoder)
    with _open(path, "wb") as raw_file, zipfile.ZipFile(raw_file, "w") as archive:
        archive.writestr("data", serialized_data)
        for filename, param in params.items():
            with archive.open(f"{filename}.npy", "w") as param_file:
                np.save(param_file, param)
        archive.writestr("_cernml_svd_version", version)


def _open(path: t.Union[str, pathlib.Path, t.IO], mode: str) -> t.IO:
    # pylint: disable = consider-using-with, unspecified-encoding
    assert "b" in mode
    if isinstance(path, t.BinaryIO):
        return path
    if isinstance(path, pathlib.Path):
        return path.open(mode)
    return open(t.cast(str, path), mode)


class NumpyGymJsonEncoder(json.JSONEncoder):
    """A JSON encoder that supports numpy-related types.

    This adds support for the following types:
    - :class:`gym.spaces.Box`,
    - :class:`numpy.dtype`,
    - :class:`numpy.number` (scalars),
    - :class:`numpy.ndarray` (arrays).

    Examples:

        >>> def roundtrip(x):
        ...     string = json.dumps(x, cls=NumpyGymJsonEncoder)
        ...     return json.loads(string, cls=NumpyGymJsonDecoder)
        >>> roundtrip(np.zeros(4, dtype=np.float32))
        array([0., 0., 0., 0.], dtype=float32)
        >>> roundtrip(gym.spaces.Box(-1, 1, (3, 3), dtype=np.int8))
        ... # doctest: +NORMALIZE_WHITESPACE
        Box([[-1 -1 -1]
             [-1 -1 -1]
             [-1 -1 -1]], [[1 1 1]
                           [1 1 1]
                           [1 1 1]], (3, 3), int8)
        >>> roundtrip([1, 2, 3])
        [1, 2, 3]
        >>> roundtrip(np.int64(132))
        132
        >>> type(roundtrip(np.float32(0.0)))
        <class 'numpy.float32'>
    """

    def default(self, o: object) -> object:
        if isinstance(o, gym.spaces.Box):
            return self._encode_box(o)
        if isinstance(o, np.ndarray):
            return self._encode_array(o)
        if isinstance(o, np.number):
            return self._encode_scalar(o)
        if isinstance(o, np.dtype):
            return self._encode_dtype(o)
        return super().default(o)

    def _encode_array(self, array: np.ndarray) -> t.Dict[str, t.Any]:
        return {
            "type": "numpy.ndarray",
            "dtype": self.encode(array.dtype),
            "value": array.tolist(),
        }

    def _encode_scalar(self, scalar: np.number) -> t.Dict[str, t.Any]:
        return {
            "type": "numpy.number",
            "dtype": self.encode(scalar.dtype),
            "value": scalar.item(),
        }

    @staticmethod
    def _encode_dtype(dtype: np.dtype) -> t.Dict[str, t.Any]:
        # Use the Pickle protocol to serialize a dtype, but throw away
        # the JSON-incompatible parts.
        func, args, state = t.cast(
            t.Tuple[t.Callable, t.Tuple, t.Tuple],
            dtype.__reduce__(),
        )
        assert func is np.dtype
        return {
            "type": "numpy.dtype",
            "args": args,
            "state": state,
        }

    def _encode_box(self, space: gym.spaces.Box) -> t.Dict[str, t.Any]:
        return {
            "type": "gym.spaces.Box",
            "low": self.encode(space.low),
            "high": self.encode(space.high),
            "dtype": self.encode(space.dtype),
        }


class NumpyGymJsonDecoder(json.JSONDecoder):
    """A JSON decoder that reverses :py:class:`NumpyGymJsonEncoder`."""

    def __init__(self) -> None:
        super().__init__(object_hook=self._object_hook)
        self._handlers: t.Dict[str, t.Callable[[t.Dict[str, t.Any]], t.Any]] = {
            "gym.spaces.Box": self._decode_box,
            "numpy.ndarray": self._decode_array,
            "numpy.number": self._decode_scalar,
            "numpy.dtype": self._decode_dtype,
        }

    def _object_hook(self, obj: t.Dict[str, t.Any]) -> t.Any:
        handler = self._handlers.get(obj.get("type", ""))
        if handler:
            return handler(obj)
        return obj

    def _decode_array(self, obj: t.Dict[str, t.Any]) -> np.ndarray:
        assert obj["type"] == "numpy.ndarray"
        dtype = self.decode(obj["dtype"])
        assert isinstance(dtype, np.dtype)
        return np.asarray(obj["value"], dtype=dtype)

    def _decode_scalar(self, obj: t.Dict[str, t.Any]) -> np.number:
        assert obj["type"] == "numpy.number"
        dtype = self.decode(obj["dtype"])
        assert isinstance(dtype, np.dtype)
        assert issubclass(dtype.type, np.number)
        return dtype.type(obj["value"])

    @staticmethod
    def _decode_dtype(obj: t.Dict[str, t.Any]) -> np.dtype:
        assert obj["type"] == "numpy.dtype"
        dtype = np.dtype(*obj["args"])
        dtype.__setstate__(tuple(obj["state"]))  # type: ignore
        return dtype

    def _decode_box(self, obj: t.Dict[str, t.Any]) -> gym.spaces.Box:
        assert obj["type"] == "gym.spaces.Box"
        low = self.decode(obj["low"])
        assert isinstance(low, np.ndarray)
        high = self.decode(obj["high"])
        assert isinstance(low, np.ndarray)
        dtype = self.decode(obj["dtype"])
        assert isinstance(dtype, np.dtype)
        return gym.spaces.Box(low, high, dtype=dtype)
