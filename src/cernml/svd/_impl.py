"""Implementation of the SVD steering algorithm."""
import numpy as np
from cernml.rltools.buffers import StepBuffer


def find_action(
    response_matrix: np.ndarray,
    obs: np.ndarray,
    max_action_size: float = float("inf"),
) -> np.ndarray:
    """Find the optimal action via SVD.
    Args:
        response_matrix: A linear description of the system's response
            to actions.
        observation: The current state of the system.

    Returns:
        An action that would bring the observation to the ideal state
        (all zeros) if the system were perfectly linear.
    """
    # pylint: disable = invalid-name
    # Because the response matrix is real, the SVD gives us two unitary
    # matrices (U and Vstar) as well as the vector S of singular values.
    # Each one is trivial to invert:
    # - inv(U) == U.T
    # - inv(Vstar) == Vstar.T
    # - inv(diag(S)) == diag(1 / S)
    # This way, we can also pseudo-invert the entire response matrix.
    U, S, Vstar = np.linalg.svd(response_matrix, full_matrices=False)
    while True:
        # Applying the inverted response matrix on the observation gives
        # us the action that led to the observation. The negative of
        # that is the action we must apply to get a zero observation.
        inverse_response_matrix = Vstar.T @ np.diag(1.0 / S) @ U.T
        action = -(inverse_response_matrix @ obs)
        # Bonus problem: We want to avoid steering too hard. To do that,
        # we look for large actions and set their corresponding singular
        # value to a large value. This large value becomes a small value
        # after inversion; hence the large action is eliminated.
        if np.all(np.abs(action) <= max_action_size):
            break
        S[np.where(S == np.min(S))] = 999
    return action


def calculate_response_matrix(data: StepBuffer) -> np.ndarray:
    """Calculate the response matrix based on measurements.

    Args:
        data: Data as recorded by `SvdOptimizer` during its learning
            phase. Must contain at least `A` samples, where `A` is the
            number of action variables.

    Returns:
        The response matrix, which has shape `A×O`, where `A` is the
        number of action variables and `O` is the number of observables.
    """
    obs = data.get_obs()
    next_obs = data.get_next_obs()
    actions = data.get_action()
    delta_obs = next_obs - obs
    num_samples, num_steerers = actions.shape
    if num_steerers != num_samples:
        raise ValueError(
            f"need exactly {num_steerers} measurements, got {num_samples} instead"
        )
    return np.transpose(np.linalg.inv(actions) @ delta_obs)
